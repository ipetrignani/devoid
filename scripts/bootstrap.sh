#!/bin/sh
#
# Part 1: Bootstrap.
# https://docs.voidlinux.org/installation/guides/chroot.html

GREEN=$(tput setaf 2)
NORMAL=$(tput sgr0)

# prepare filesystems

wipefs -a /dev/sda
cfdisk -z /dev/sda

mkfs.vfat -F 32 /dev/sda1
mkfs.ext4 /dev/sda2

mount /dev/sda2 /mnt/
mkdir -p /mnt/boot/
mount /dev/sda1 /mnt/boot/

# base-system

ARCH=x86_64
REPO=https://alpha.de.repo.voidlinux.org/current

mkdir -p /mnt/var/db/xbps/keys
cp /var/db/xbps/keys/* /mnt/var/db/xbps/keys/

XBPS_ARCH=$ARCH xbps-install -S -r /mnt -R "$REPO" void-repo-multilib \
    void-repo-multilib-nonfree \
    void-repo-nonfree

XBPS_ARCH=$ARCH xbps-install -S -r /mnt -R "$REPO" base-system \
    intel-ucode \
    mksh \
    opendoas \
    openntpd \
    syslinux \
    terminus-font

# prepare chroot environment

mount --rbind /sys /mnt/sys && mount --make-rslave /mnt/sys
mount --rbind /dev /mnt/dev && mount --make-rslave /mnt/dev
mount --rbind /proc /mnt/proc && mount --make-rslave /mnt/proc

# chroot

PROJECT=devoid

cp -r "$PROJECT" /mnt/
xchroot /mnt/ sh "$PROJECT"/scripts/chroot.sh
