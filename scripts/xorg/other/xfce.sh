#!/bin/sh
#
# XFCE.

# feed the good stuff

doas xbps-install -S ImageMagick \
  base-devel \
  curl \
  ffmpeg \
  firefox \
  git \
  gvfs \
  intel-media-driver \
  iucode-tool \
  mesa-dri \
  mksh \
  p7zip \
  p7zip-unrar \
  wget \
  xarchiver \
  xfce4 \
  xfce4-plugins \
  xorg-minimal \
  xtools

doas xbps-remove -oOR

doas makewhatis -a

chsh -s /bin/mksh

# config files

project=voidance

cp "$HOME"/"$project"/.gitconfig "$HOME"
cp "$HOME"/"$project"/.mkshrc "$HOME"
cp "$HOME"/"$project"/.vimrc "$HOME"

doas cp -r "$HOME"/"$project"/etc/X11/xorg.conf.d /etc/X11/

# XFCE theme

icons=/usr/share/icons

equilux=equilux-v20181029
mcmojave=https://github.com/vinceliuice/McMojave-cursors/archive/master.tar.gz

wget https://github.com/ddnexus/equilux-theme/archive/"$equilux".zip
unzip "$equilux".zip -d "$equilux"
doas ./"$equilux"/install.sh

wget -O master.zip https://github.com/numixproject/numix-icon-theme/archive/master.zip
unzip master.zip
doas mv numix-icon-theme-master/Numix/ "$icons"

wget -O master.zip https://github.com/numixproject/numix-icon-theme-circle/archive/master.zip
unzip master.zip 
doas mv numix-icon-theme-circle-master/Numix-Circle/ "$icons"

wget https://github.com/vinceliuice/McMojave-cursors/archive/master.tar.gz
tar xf master.tar.gz
doas ./McMojave-cursors-master/install.sh

# services

doas ln -s /etc/sv/dbus /var/service

# rootless Xorg

doas sed -i 's/yes/no/g' /etc/X11/Xwrapper.config

doas rm -rf "$project" *master*
doas reboot
