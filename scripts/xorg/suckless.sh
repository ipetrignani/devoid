#!/bin/sh
#
# Suckless.

chsh -s /bin/mksh
# feed the good stuff
doas xbps-install -Sy \
  alsa-utils \
  base-devel \
  brightnessctl \
  curl \
  dejavu-fonts-ttf \
  ffmpeg \
  git \
  htop \
  intel-media-driver \
  libX11-devel \
  libXext-devel \
  libXft-devel \
  libXinerama-devel \
  mesa-dri \
  mksh \
  mpv \
  p7zip \
  p7zip-unrar \
  vim \
  wget \
  xorg-minimal \
  xtools \
  zathura \
  zathura-pdf-mupdf

doas xbps-remove -oOR
doas makewhatis -a

# config files
PROJECT=devoid

cp "$HOME"/"$PROJECT"/.* "$HOME"
cp -r "$HOME"/"$PROJECT"/.config/ "$HOME"

curl -L https://github.com/yt-dlp/yt-dlp/releases/latest/download/yt-dlp -o "$HOME"/.config/mpv/yt-dlp
chmod +x "$HOME"/.config/mpv/yt-dlp

# sucking less...

mkdir "$HOME"/.suckless/
cd "$HOME"/.suckless/ || exit

git init

for s in dmenu dwm scroll slstatus st; do
  git submodule add git://git.suckless.org/"$s"/ "$s"/
  cp "$HOME"/"$PROJECT"/.suckless/"$s"/config.h "$s"/
  doas make -j4 install -C "$s"/
done

cd "$HOME" || exit

# theme
mkdir -p "$HOME"/.local/share/krita/color-schemes/
cp "$HOME"/"$PROJECT"/.local/share/color-schemes/* "$HOME"/.local/share/krita/color-schemes/

wget https://github.com/ddnexus/equilux-theme/archive/equilux-v20181029.zip
unzip equilux-v20181029.zip
doas ./equilux-theme-equilux-v20181029/install.sh
rm -rf equilux-*

wget -qO install.sh https://git.io/papirus-icon-theme-install
sed -i 's/sudo/doas/g' install.sh
sh install.sh

wget -qO install.sh https://git.io/papirus-folders-install
sed -i 's/sudo/doas/g' install.sh
sh install.sh

doas papirus-folders -C grey --theme Papirus-Dark
rm install.sh

# vim packages
mkdir -p "$HOME"/.vim/pack/foo/start/
cd "$HOME"/.vim/pack/foo/start/ || exit

git init

git submodule add https://github.com/tpope/vim-commentary
git submodule add https://github.com/tpope/vim-flagship
git submodule add https://github.com/tpope/vim-fugitive
git submodule add https://github.com/tpope/vim-repeat
git submodule add https://github.com/tpope/vim-sleuth
git submodule add https://github.com/tpope/vim-surround
git submodule add https://github.com/tpope/vim-vinegar
git submodule add https://github.com/dense-analysis/ale

vim -c "helptags ALL" -cq

cd "$HOME" || exit

# rootless Xorg
doas sed -i 's/yes/no/g' /etc/X11/Xwrapper.config

# cleanup
doas rm -rf "$HOME"/"$PROJECT"/
doas reboot
